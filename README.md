# Website Monitoring Project

This project aims to automate the monitoring and management of a website hosted on an AWS server using Docker containers and Nginx. The system consists of a Python program for monitoring the application, sending email alerts when the website is down, and automating the process of restarting Docker containers and the server when necessary.

## Preparation Steps

1. **Create an AWS Server**: Set up an AWS server to host the website.
   
2. **Install Docker**: Install Docker on the AWS server to manage and run containerized applications.

3. **Run Nginx Container**: Run an Nginx container on the Docker to serve the website.

## Automation Program

The automation program is written in Python and performs the following tasks:

1. **Website Monitoring**: Continuously checks the status of the application.
   
2. **Email Alerts**: Sends an email notification when the website is detected as down.

3. **Fixing Problems**:
   - **Restart Docker Container**: Automatically restarts the Docker container hosting the website.
   - **Restart Server**: If restarting the Docker container doesn't resolve the issue, the program restarts the entire server.

## Prerequisites

- Python 3.x installed on the AWS server.
- Docker installed on the AWS server.

## Setup

1. Clone the repository to your AWS server.

   ```bash
   git clone <repository_url>
   ```

2. Install the required Python packages.

   ```bash
   pip install -r requirements.txt
   ```

3. Configure the email settings in `config.py` file.

4. Run the Python program.

   ```bash
   python website_monitor.py
   ```

## Configuration

- Update the `config.py` file with your email server settings, recipient email address, and any other relevant configurations.

## Usage

- Once the program is running, it will continuously monitor the website.
- If the website goes down, an email alert will be sent.
- The program will attempt to restart the Docker container.
- If the issue persists, the server will be restarted.

## Notes

- Ensure that the Docker container is set up to automatically restart on failure.
- Monitor the email inbox configured in `config.py` for alerts.
- Customize the program further as per specific requirements or environments.

## Contributors

- [Your Name] - [Your Email] - [Your Website]

## License

This project is licensed under the [MIT License](LICENSE).